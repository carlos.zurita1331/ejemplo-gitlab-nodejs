
//estoy importando el paquete de colors
require('colors')
//estoy importando el file System
const fs = require('fs');

const crearArchivo = async (base = 5) =>{
  try {
    let salida = ''
    console.log('====================='.green);
    console.log(` tabla del ${base}   `.green);
    console.log('====================='.green);
  
    for(let i = 1; i <= 10; i++){
      salida +=`${base} x ${i} = ${base * i}\n`;
    }
  
    console.log(salida);
    fs.writeFileSync(`tabla ${base}.txt`,salida);
     return `tabla-${base}.txt`;

      
    } catch (error) {
      throw(error)
  }

};
//exportamos el archivo
module.exports={
  generarArchivo:crearArchivo
};
